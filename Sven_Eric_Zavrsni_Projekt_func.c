#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Sven_Eric_Zavrsni_Projekt_head.h"

void Izbornik(void)                                                                                        //Glavni izbornik
{
	int a = 0;
	do
	{
	    printf("Odaberite opciju:\n1. Pokreni igru\n2. Ljestvica\n3. Upute za igranje\n4. Izlaz\n\n");
	    scanf("%d", &a);
		switch (a)
		{
		case 1:
		{
			int BrojBodova = Hangman();                  //case 1 pokrece Hangman igru
		}; break;
		case 2:
		{ 
			Ljestvica();                                //case 2 prikazuje ljestvicu
		}; break;
		case 3:
		{
			Upute();                                   //case 3 prikazuje pravila Hangman igre
		}; break;
		case 4:
		{
			Izlaz();                                  //case 4 Izlazi iz programa
		}; break;
		default:
		{
			system("cls");
			while ((getchar()) != '\n');
			printf("Molim da unesete broj izmedu 1 i 4\n\n");                 //ako upisemo nesto drugo zamolit ce nas da upisemo broj izmedu 1 i 4
		};
		}
	} while (1);
}


int Hangman(void)
{
	system("cls");
	while ((getchar()) != '\n');
	printf("Dobrodosli u igru vjesala!\nMozete upisati rijec 'izlaz' za povratak u izbornik\n\n");
	srand(time(NULL));
	char RandomRijeci[][16] = { "avion", "zagreb", "osijek", "ferit", "patka", "rijeka", "europa" };        //sve rijeci koje se mogu pojaviti u igri Hangman
	int RandomIndeks = rand() % 7;                                           //random odabir navedenih rijeci
	int BrojZivota = 5;
	int BrojBodova = 0;
	int BrojTocnih = 0;
	int StariTocni = 0;                                                //pomoc pri odredivanju netocnih slova
	int DuljinaRijeci = strlen(RandomRijeci[RandomIndeks]);
	int Pogadanje[7] = { 0, 0, 0, 0, 0, 0, 0 };                       //polje pri kojem se gleda individualno slovo u rijeci
	int izlaz = 0;
	int i = 0; 
	char Pogodak[16];                                                 //Unos
	char UnesenoSlovo;
	while (BrojTocnih < DuljinaRijeci)                      //hangman igra, sve dok je broj pogodenih slova manji od duljine slova rijeci nismo pogodili cijelu rijec
	{
		printf("\n\n\n");
		for (i = 0; i < DuljinaRijeci; i++)
		{
			if (Pogadanje[i] == 1)
			{
				printf("%c ", RandomRijeci[RandomIndeks][i]);         //ako smo pogodili slovo ono ce biti ispisano na konzoli
			}
			else
			{
				printf("_ ");                                         //u suprotnom ostaju prazna mjesta
			}
		}
		printf("\n\nBroj zivota: %d\n", BrojZivota);
		printf("Broj bodova: %d\n\n", BrojBodova);
		printf("Unesite slovo: ");
		fgets(Pogodak, 16, stdin);
		system("cls");
		if (strncmp(Pogodak, "izlaz", 5) == 0)                       //unosenje rijeci "izlaz" izlazimo iz while petlje
		{
			izlaz = 1;
			break;
		}
		UnesenoSlovo = Pogodak[0];
		StariTocni = BrojTocnih; 
		for (i = 0; i < DuljinaRijeci; i++)                          //for petlja vrti za duljinu rijeci
		{
			if (Pogadanje[i] == 1)
			{
				continue;
			}
			if (UnesenoSlovo == RandomRijeci[RandomIndeks][i])        //ako se uneseno slovo podudara sa nekim slovom u rijeci pogodili smo slovo i dobili bodove
			{
				Pogadanje[i] = 1;
				BrojTocnih++;
				BrojBodova += 65;
			}
		}
		if (StariTocni == BrojTocnih)                                 //ako broj pogodenih slova ostane isti to znaci da nismo pogodili slovo i oduzimaju nam se bodovi
		{
			BrojZivota--;
		    BrojBodova -= 35;
			if (BrojZivota == 0)                                      //ako nam ponestane zivota izgubili smo
			{
				break;
			}
		}
	}
	if (izlaz == 1)                                                          //izlazenje iz programa
	{
		system("cls");
		Izbornik();
	}
	else if (BrojZivota == 0)                                                    //ako nam ponestane pokusaja(zivota) izgubili smo
	{
		system("cls");
		printf("Izgubili ste, rijec je bila: %s\n", RandomRijeci[RandomIndeks]);
		PokusajPonovno(BrojBodova);
	}
	else                                                                              //u suprotnom smo pobijedili
	{
		system("cls");
		printf("CESTITAM, POBIJEDILI STE!!\nRijec je bila: %s\n", RandomRijeci[RandomIndeks]);
		PokusajPonovno(BrojBodova);
	}
	return BrojBodova;
}


void Upute(void)         //pravila/upute za igranje vjesala
{
	system("cls");
	printf("\nHangman (vjesala) je igra pogadanja slova odredene rijeci u sto manje pokusaja bez da korisnik ostane bez zivota,\nkorisnik je pobijedio ako pogodi sva slova odredene rijeci bez da izgubi sve zivote\n\n");
	NatragNaIzbornik();
}


void Izlaz(void)
{
	char da[] = "da";
	char ne[] = "ne";
	char odluka[3];
	printf("Jeste li sigurni zelite li izaci iz igre?\nda/ne\n\n");
	scanf("%s", &odluka);
	if (_strcmpi(da, odluka) == 0)                                        //ako upisemo "da" izaci cemo potpuno iz programa
	{
		exit(EXIT_SUCCESS);
	}
	if (_strcmpi(ne, odluka) == 0)                                       //ako upisemo "ne" vracamo se natrag u izbornik
	{
		system("cls");
		Izbornik();
	}
	else
	{
		system("cls");
		printf("\nUpisite 'da' ili 'ne'\n");                          //ako napisemo nesto sto nije "da" ili "ne" dobit cemo ovu poruku i ponovno cemo moci napisati "da" ili "ne"
		Izlaz();
	}
}


void UpisBodova(int BrojBodova)
{
	FILE* Test = NULL;
	Test = fopen("Ljestvica.bin", "rb");                    //Program provjerava postoji li Ljestvica.bin 
	if (Test == NULL)
	{
		fflush(Test);
		Test = fopen("Ljestvica.bin", "wb");                //Ako ne postoji Ljestvica.bin program ju napravi, u suprotnom ako vec postoji Ljestvica.bin program ide dalje
		int Broj = 0;
		fwrite(&Broj, sizeof(int), 1, Test);
	}
	fclose(Test);
	PLAYER* igrac = NULL;
	igrac = (PLAYER*)calloc(1, sizeof(PLAYER));
	FILE* fp = NULL;
	fp = fopen("Ljestvica.bin", "rb+");
	if (fp == NULL)
	{
		perror("Otvaranje");
	}
	else
	{
		int x = 0;
		fread(&x, sizeof(int), 1, fp);
		x++;
		fseek(fp, 0, SEEK_SET);
		fwrite(&x, sizeof(int), 1, fp);
		fseek(fp, 0, SEEK_END);
		printf("Osvojeni bodovi: %d\n", BrojBodova);
		printf("Unesite vase ime: ");
		scanf(" %[^\n]%*c", igrac->ime);
		igrac->bodovi = BrojBodova;
		fwrite(igrac, sizeof(PLAYER), 1, fp);
		system("cls");
	}
	fclose(fp);
}


void PokusajPonovno(int BrojBodova)
{
	char da[] = "da";
	char ne[] = "ne";
	char odluka[3];
	printf("\nBroj bodova: %d\n", BrojBodova);
	printf("Pokusaj ponovno?\nda/ne\n\n");                   //Opcija da pokusamo ponovno odigrati Hangman ako nismo zadovoljni s dobivenim bodovima
	scanf("%s", &odluka);
	if (_strcmpi(da, odluka) == 0)                            //ako upisemo "da" ponovno se pokrece Hangman
	{
		Hangman();
	}
	if (_strcmpi(ne, odluka) == 0)                           //ako upisemo "ne" program ide dalje s upisivanjem bodova i korisnickog imena
	{
		UpisBodova(BrojBodova);
	}
	else
	{
		system("cls");
		printf("\nUpisite 'da' ili 'ne'\n\n");              //ako upisemo nesto sto nije "da" ili "ne" 
		PokusajPonovno(BrojBodova);
	}
}


void Ljestvica(void)
{
	system("cls");
	FILE* fp = NULL;
	fp = fopen("Ljestvica.bin", "rb+");
	if (fp == NULL)
	{
		perror("Error");
		printf("Nema upisanih igraca");                 //ako pokusamo otvoriti ljestvicu dok nema upisanih igraca dobit cemo poruku da trenutacno nema igraca i vratit ce nas u izbornik
		NatragNaIzbornik();
	}
	else
	{
		int k = 0;
		fread(&k, sizeof(int), 1, fp);
		PLAYER* igrac = NULL;
		igrac = (PLAYER*)calloc(k, sizeof(PLAYER));
		fread(igrac, sizeof(PLAYER), k, fp);
		int x = 0;
		do
		{
			while ((getchar()) != '\n');
			BrojIgraca();
			printf("Zelite li poredati po:\n1. imenima\n2. bodovima\n3. Pretrazi igraca\n4. Nazad\n\n\n");
			scanf("%d", &x);
			switch (x)
			{
			case 1:
			{
				system("cls");
				SelectionSortIme(igrac, k);
				IspisLjestviceIme(igrac, k);
				NatragNaIzbornik();
			} break;
			case 2:
			{
				system("cls");
				SelectionSortBodovi(igrac, k);
				IspisLjestviceBodovi(igrac, k);
				NatragNaIzbornik();
			}break;
			case 3:
			{
				system("cls");
				PretrazivanjeIgraca();
			}break;
			case 4:
			{
				system("cls");
				Izbornik();
			}
			default:
			{
				system("cls");
				printf("Molim da unesete broj izmedu 1 i 3\n\n");
			};
			}
		} while (1);
	}
}


void SelectionSortBodovi(PLAYER* igrac, const int k)
{
	int min = 0;
	for (int i = 0; i < k - 1; i++)
	{
		min = i;
		for (int j = i + 1; j < k; j++)
		{
			if ((igrac + j)->bodovi > (igrac + min)->bodovi) 
			{
				min = j;
			}
		}
		Zamjena((igrac + i), (igrac + min));
	}
}


void SelectionSortIme(PLAYER* igrac, const int k)
{
	int min = 0;
	for (int i = 0; i < k - 1; i++)
	{
		min = i;
		for (int j = i + 1; j < k; j++)
		{
			if (strcmp((igrac + j)->ime, (igrac + min)->ime) < 0) 
			{
				min = j;
			}
		}
		Zamjena((igrac + i), (igrac + min));
	}
}


void Zamjena(PLAYER* veci, PLAYER* manji) 
{
	PLAYER temp = *manji;
	*manji = *veci;
	*veci = temp;
}


void IspisLjestviceIme(PLAYER* igrac, const int k)
{
	int i;
	for (i = 0; i < k; i++)
	{
		printf("\n%d. Ime: %s   Bodovi: %d\n", (i + 1), (igrac + i)->ime, (igrac + i)->bodovi);
	}
}


void IspisLjestviceBodovi(PLAYER* igrac, const int k)
{
	int i;
	for (i = 0; i < 10; i++)
	{
		printf("\n%d. Ime: %s   Bodovi: %d\n", (i + 1), (igrac + i)->ime, (igrac + i)->bodovi);
	}
}


void NatragNaIzbornik(void)
{
	while ((getchar()) != '\n');
	int p = 0;
	printf("\nPritisnite broj 1 za povratak u izbornik  ");
	scanf("%d", &p);
	if (p == 1)
	{
		system("cls");
		Izbornik();
	}
	else
	{
		system("cls");
		printf("\nPogresan unos, pokusajte ponovno\n\n");
		NatragNaIzbornik();
	}
}


void BrojIgraca(void)
{
	FILE* fp;
	fp = fopen("Ljestvica.bin", "rb+");
	rewind(fp);
	if (fp == NULL)
	{
		perror("Otvaranje");
	}
	else
	{
		fseek(fp, 0, SEEK_END);
		int broj = ftell(fp) / sizeof(PLAYER);                                //cita broj zapisanih igraca u strukturi
		printf("Trenutacni broj igraca: %d\n\n", broj);
	}
	fclose(fp);
}


void PretrazivanjeIgraca(void)
{
	FILE* fp = NULL;
	fp = fopen("Ljestvica.bin", "rb+");
	if (fp == NULL)
	{
		perror("Otvaranje");
	}
	fseek(fp, 0, SEEK_END);
	fseek(fp, sizeof(int), SEEK_SET);
	char s[20];
	printf("Unesite ime igraca: ");
	scanf(" %[^\n]%*c", s);
	int k = 0;
	while (fread(&igrac, sizeof(igrac), 1, fp) == 1)
	{
		if (_strcmpi(igrac.ime, (s)) == 0)                                                    //usporeduje uneseno ime s imenima u strukturi
		{
			printf("\nIme: %s\nBroj bodova: %d\n", igrac.ime, igrac.bodovi);
			k++;                                                                            //ako je ime pronadeno 'k' se poveca za 1
		}
	}
	if (k == 0)                                                                            //ako se 'k' nije povecao to znaci da ime nije pronadeno
	{
		printf("Ime nije pronadeno");
	}
	PonovnoPretrazi();
	fclose(fp);
}


void PonovnoPretrazi(void)
{
	char da[] = "da";
	char ne[] = "ne";
	char odluka[3];
	printf("\n\nZelite li ponovo pretraziti igraca?\nda/ne\n");
	scanf("%s", &odluka);
	if (_strcmpi(da, odluka) == 0)                                                 //ako upisemo 'da' ponovno se pokrece funkcija za pretrazivanje igraca
	{
		system("cls");
		PretrazivanjeIgraca();
	}
	if (_strcmpi(ne, odluka) == 0)                                                //ako upisemo 'ne' vracamo se natrag u izbornik
	{
		system("cls");
		Izbornik();
	}
	else
	{
		system("cls");
		printf("Molim vas unesite 'da' ili 'ne'\n\n");
		PonovnoPretrazi();
	}
}