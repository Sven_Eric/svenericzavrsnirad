#ifndef SVEN_ERIC_ZAVRSNI_PROJEKT_HEAD
#define SVEN_ERIC_ZAVRSNI_PROJEKT_HEAD

typedef struct
{
	char ime[20];
	int bodovi;
}PLAYER;
PLAYER igrac;

void Izbornik(void);
int Hangman(void);
void Upute(void);
void Izlaz(void);
void UpisBodova(int);
void PokusajPonovno(int);
void Ljestvica(void);
void Zamjena(PLAYER*, PLAYER*);
void SelectionSortIme(PLAYER*, const int);
void SelectionSortBodovi(PLAYER*, const int);
void IspisLjestviceIme(PLAYER*, const int);
void IspisLjestviceBodovi(PLAYER*, const int);
void NatragNaIzbornik(void);
void BrojIgraca(void);
void PretrazivanjeIgraca(void);
void PonovnoPretrazi(void);

#endif